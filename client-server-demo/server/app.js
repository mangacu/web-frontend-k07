const express = require('express');
const app = express();
const port = 3000;

var mongoose = require('mongoose');
mongoose.connect("mongodb://admin:abc123@ds053529.mlab.com:53529/ajax-demo");

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("connected");
});


var userSchema = new mongoose.Schema({
    name: String,
    isActive: Boolean,
    username: String
});
var User = mongoose.model('User', userSchema);


app.post('/user', (req, res) => {
    console.log('post user');
    var user = new User({name: "test", isActive: true, username: "test user name"});

    user.save((err, userRes) => {
        if (err) {
            res.status(400).res(err);
            return;
        } 
        res.send(userRes);
    })
})

app.get('/user', (req, res) => {
    console.log("get user");
    User.find((err, users) => {
        if (err) {
            res.status(404).res(err);
            return;
        }
        res.send(users);
    })
});

app.get('/user/:id', (req, res) => {
    console.log("get user by id", req.params.id);
    User.find({_id: req.param.id}, (err, users) => {
        if (err) {
            res.status(404).res(err);
            return;
        }
        res.send(users);
    })
});

app.delete('/user/:id', (req, res) => {
    
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));