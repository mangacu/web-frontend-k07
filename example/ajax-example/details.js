$(document).ready(function() {


    var url = window.location.href;
    var arrString = url.split("?");

    var parametersString = arrString[1];

    console.log(parametersString);

    var paramsObj = getParamsObjFromString(parametersString);

    // id=1&name=abc

    console.log(paramsObj);


    console.log(paramsObj.id);



    $.ajax({
        url: "https://www.jasonbase.com/things/aKe6.json",
        type: "GET"
    }).done(function(objResponse) {
        
        var arrayUser = objResponse.arrayUser;

        for(var i=0; i<arrayUser.length; i++) {
            var user = arrayUser[i];

            if (user.id.toString() == paramsObj.id) {

                var htmlString = `<h1>${user.userName}</h1>
                <p>${user.isActive}</p>
                <p>${user.scannerUsername}</p>`
        
                $("#user-details").html(htmlString);

                break;
            }

        }
    })


    


    function getParamsObjFromString(paramsString) {
        var arrStr = paramsString.split("&");

        // ["id=1", "name=abc"]

        var obj = {};

        for(var i=0; i<arrStr.length; i++) {
            var str = arrStr[i];
            // id=1
            var arr = str.split("=");

            // arr[0] = "id"
            // arr[1] = "1"
            // arr["id"] = "1"

            obj[arr[0]] = arr[1];
        }

        return obj;
    }

})