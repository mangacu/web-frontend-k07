// var hello = document.getElementById("hello");
// console.log(hello);




// variableExample();

// ifElseExample();

switchCaseExample();


function variableExample() {
    var so = 1;

    console.log(typeof (so));

    var chuoi = 'hom nay toi di hoc';

    console.log(typeof (chuoi));

    var dungsai = true;

    console.log(typeof (dungsai));

    so = "2";

    console.log(typeof (so));


    var hocsinh = {
        name: "Nguyen Van A",
        age: 25
    }

    console.log(typeof (hocsinh));

    var so1 = 1;
    var so2 = 2;

    so++;
    so = so + 1;

    so += 5;

    so1 + so2;


    var bienUndefined;

    console.log(typeof (bienUndefined))

    var bienNull = null;

    console.log(typeof (bienNull));
}




var biencuatoi = 12;

function localFunction() {
    var biencuatoi = 13;

    console.log(biencuatoi);
}



function ifElseExample() {
    var tenNuocUong = "lavi";
    var giatien;

    // pepsi - 8000
    // coca - 9000
    // aquafina - 5000

    if (tenNuocUong == "pepsi") {
        giatien = "8000";
    } else if (tenNuocUong == "coca") {
        giatien = "9000"
    } else if (tenNuocUong == "aquafina") {
        giatien = "5000"
    } else {
        giatien = "quan khong co ban";
    }

    console.log(giatien);
}



function switchCaseExample() {
    var tenNuocUong = "coca";
    var giatien;

    switch (tenNuocUong) {
        case 'pepsi':
            console.log("8000");
            break;
        case "coca":
            console.log("9000")
            break;
        case "aquafina":
            console.log("5000")
            break;
        default:
            console.log("quan khong ban")
    }

}

