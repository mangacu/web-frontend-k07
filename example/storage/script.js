function pushTosessionStorage() {


    var giohang = [
        {
            id_giohang: 10,
            soluong: 11
        },
        {
            id_giohang: 11,
            soluong: 2
        }
    ];

    console.log(giohang);
    console.log(typeof(giohang));

    var giohangString = JSON.stringify(giohang);

    console.log(giohangString);
    console.log(typeof(giohangString));

    window.sessionStorage.setItem("giohang", giohangString);


}

function getFromsessionStorage() {
    var giatri = window.sessionStorage.getItem("giohang");
    console.log(giatri);
    console.log(typeof(giatri));

    var giohangObj = JSON.parse(giatri);

    console.log(giohangObj);
    console.log(typeof(giohangObj))


    for(var i=0; i<giohangObj.length; i++) {
        console.log(giohangObj[i].id_giohang);
    }

    
}

function removeFromsessionStorage() {
    window.sessionStorage.removeItem("name");
}

// window.sessionStorage.clear()


function buy(id) {
    var giohang = window.sessionStorage.getItem("giohang");

    // chua co gio hang
    // tao moi gio hang voi thong tin product vua mua
    if (!giohang) {
        console.log("chua co gio hang, them moi");
        var giohangArr = [{
            id_monhang: id,
            soluong: 1
        }];
        window.sessionStorage.setItem("giohang", JSON.stringify(giohangArr));
    }
    // da co gio hang trong sessionStorage
    // them vao gio hang 
    else {
        console.log("da co gio hang, them sp vao gio hang");
        var giohangArr = JSON.parse(giohang);

        var indexFromGiohangArr = getIndexFromGiohangArray(giohangArr, id);

        if (indexFromGiohangArr >= 0) {
            // tang so luong
            giohangArr[indexFromGiohangArr].soluong++;
        } else {
            // tao mot object moi va them vao mang gio hang
            var product = {
                id_monhang: id,
                soluong: 1
            }
            giohangArr.push(product);
        }

        window.sessionStorage.setItem("giohang", JSON.stringify(giohangArr));
    }
}

function getIndexFromGiohangArray(giohangArr, id) {
    for(var i=0; i<giohangArr.length; i++) {
        if (giohangArr[i].id_monhang == id) {
            return i;
        }
    }
    return -1;
}